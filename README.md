# TPprotocoleHttp
# Pour être sur un env virtuel
venv/scipts/activate


Notes du cours : 
---------------------
Les échanges sont toujours à l'initiative du client
Le client fait une requête, le serveur répond à la requête
Client et serveur ne parlent jamais en même temps : le client parle puis écoute tandis que le serveur écoute puis parle
HTTP est un protocole "déconnécté", chaque requête est indépendante
Astuce: on peut taper nos requêtes sur Insomnia pour voir si elles fonctionnent c'est a dire une requete qui renvoie un statut 200.

Deux parties dans ce TP:
----------------------- 

Partie client: /Client

Partie serveur: /Serveur


Mettre la configuration dans l'environnement
-----------
Définir un fichier .env que on ne va pas forcèment versionner. Facilite la portabilité du code et évite de dilapider des secrets. 
usage : importe configuration avec le nom du fichier. 
