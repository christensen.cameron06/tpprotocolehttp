import requests

from data import mots
from metier import Mot

from fastapi import FastAPI

#import requests

app = FastAPI()

@app.get("/")
def read_root():
    return {"hello"}



#Notre serveur peut répondre à du get et du post

@app.get("/mots")
def get_all_words():
         return mots


#On définit une méthode et faut qu'on puisse lire les informations, soit on éclate les infos, soit on crée la structure du mot

@app.post("/mot/")
async def add_word(mot:Mot):
    mots.append(mot)
    return mot
